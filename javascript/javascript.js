
function setting() {
    document.getElementById("mySidenav").style.width = "280px";
    document.getElementById("opt").setAttribute('onclick','closeNav()');
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0px";
    document.getElementById("opt").setAttribute('onclick','setting()')
}


function dark() {
	document.getElementById('theme').href="style/dark.css";
    document.getElementById("theme_toggle").setAttribute('onclick','light()');
}

function light() {
    document.getElementById('theme').href="style/main.css";
    document.getElementById("theme_toggle").setAttribute('onclick','dark()')
}
